@extends("superbase")
@section("content")
@include("_nav")
<div class="container">
    @if(session("message"))
    <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert" style="margin-top: 20px">
      <strong>Attenzione:</strong> {{session("message")}}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
    @endif

@yield("buco")

</div>
@endsection   