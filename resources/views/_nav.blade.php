<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('home')}}">ChefMil</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('piatti')}}">Ricette <span class="badge badge-danger">{{session("n_piatti",0)}}</span></a>
      </li>
     <li class="nav-item">
        <a class="nav-link" href="{{route('formpiatto')}}">Aggiungi</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" action="{{route('cercapiatto')}}" method="POST">
      @csrf
      <input class="form-control mr-sm-2" type="search" placeholder="Cerca Piatto" aria-label="Search" name="piatto">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cerca</button>
    </form>
  </div>
</nav>