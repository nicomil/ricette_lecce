@extends("base")
@section("buco")
<div class="row">
	<div class="col-12 text-center">
		<h1>Nuova Ricetta: fai del tuo meglio!</h1>
	</div>
</div>
<div class="row">
	<div class="col"></div>
	<div class="col">
		<form action="/plate" method="POST">
			@csrf
			<div class="form-group">
				<label >Piatto</label>
				<input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Nome piatto" name="piatto">
				<label >Ingredienti (separati da virgola)</label>
				<input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Ingredienti" name="ingredienti">
				<label >Immagine (url)</label>
				<input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Immagine" name="immagine">
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
	<div class="col"></div>
</div>
@endsection