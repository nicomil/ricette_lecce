@extends("base")
@section("buco")
<div class="row">
    <div class="col-3"></div>
    <div class="col-6">
        <div class="jumbotron" style="margin-top:50px">
            <img src="https://www.illibraio.it/wp-content/uploads/2018/02/cibo-981x540.jpg" alt="" class="img-fluid">
            <h1 class="display-4">ChefMIL</h1>
            <p class="lead">Cucinare non è mai stato così facile! Anni e anni di esperienza al vostro servizio :)</p>
            <hr class="my-4">
            <p>La raccolta delle migliori ricette del mondo, aggiungi la tua o prova a cucinarne qualcuna!</p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="{{route('piatti')}}" role="button">Vai alle ricette</a>
            </p>
        </div>
    </div>
    <div class="col-3">
        <div class="jumbotron" style="margin-top:50px">
            <h1 class="display-5">Contattamil</h1>
            <form action="{{route('contatto')}}" method="POST">
                @csrf
            <div class="form-group">
                <label >Email</label>
                <input type="email" class="form-control"  aria-describedby="emailHelp" placeholder="Inserisci Email" name="email">
            </div>
            <div class="form-group">
                <label >Messaggio</label>
                <textarea class="form-control"  aria-describedby="emailHelp" placeholder="Inserisci Messaggio" name="messaggio">
                </textarea>
            </div>
                <button class="btn btn-outline-success" type="submit">Invia</button>
            </form>
        </div>
    </div>
</div>
@endsection