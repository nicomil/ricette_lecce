@extends("base")
@section("buco")
<div class="row">
	<div class="col-12">
		<h1>{{$plate['nome']}}</h1>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<img src="{{$plate['immagine']}}" class="img-fluid" alt="">
	</div>
	<div class="col-6">
		<strong>Ricetta #{{$plate['id']}}: {{$plate['nome']}}</strong>
		<ul>
			@foreach($plate['ingredienti'] as $ingrediente) 
			<ol>
			{{$ingrediente}}
			</ol>
			 @endforeach
		</ul>
	</div>
</div>

@endsection