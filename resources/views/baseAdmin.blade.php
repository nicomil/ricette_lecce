@extends("superbase")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-3">
             @yield("buco-laterale")
            </div>
            <div class="col-9">
             @yield("buco")
            </div>
        </div>
    </div>
@endsection  