@extends("base")
@section("buco")
<div class="row">
	<div class="col-12 text-center">
		<h1>Ricette del mondo</h1>
	</div>
</div>
<div class="row">
	@if($plates)
	@foreach($plates as $plate)
	<div class="col-4">
	<div class="card" style="width: 18rem;margin-top: 20px">
		<img class="card-img-top" src="{{$plate['immagine']}}" alt="Card image cap">
		<div class="card-body">
			<h5 class="card-title">{{$plate['nome']}}</h5>
			<p class="card-text">Ingredienti: @foreach($plate['ingredienti'] as $ingrediente) {{$ingrediente}} @endforeach

			</p>
			<a href="/plate/{{$plate['id']}}" class="btn btn-primary">Vedi</a>
		</div>
	</div>
	</div>
	@endforeach
	@endif
	<div class="col-4">
	<div class="card" style="width: 18rem;margin-top: 20px">
		<a href="{{route('formpiatto')}}">
			<img class="card-img-top" src="http://freevector.co/wp-content/uploads/2014/09/87345-add-symbol.png" alt="Card image cap">
		</a>
		<div class="card-body">
			<h5 class="card-title">Nuovo Piatto</h5>
			<p class="card-text">Ingredienti: Aggiungi i tuoi ingredienti

			</p>
			<a href="{{route('formpiatto')}}" class="btn btn-primary">Nuova</a>
		</div>
	</div>
	</div>
</div>

@endsection