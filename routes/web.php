<?php

use App\Mail\NewContact;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MioController@home')->name("home");
Route::get('/users', 'MioController@users');

//Rotte dei piatti
Route::get('/plates', 'PlatesController@showplates')->name("piatti");
Route::get('/plate/{id}', 'PlatesController@showplate')->name("vedipiatto");
Route::get('/plate', 'PlatesController@formplate')->name("formpiatto");
Route::post('/plate', 'PlatesController@addplate')->name("aggiungipiatto");
Route::get('/flush', 'PlatesController@flush');
Route::post('/search','PlatesController@search')->name("cercapiatto");
Route::post('/new-contact','MioController@newcontact')->name("contatto");

Route::get('/test-mail',function(){
	Mail::to("nicombk@gmail.com")->send(new NewContact());
});