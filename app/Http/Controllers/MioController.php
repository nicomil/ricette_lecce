<?php

namespace App\Http\Controllers;

use App\Mail\NewContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MioController extends Controller
{
    public function home(){
    	return view("welcome");
    }

    public function users(){
    	return view("users");
    }

    public function plates(){
    	return view("piatti");
    }

    public function newcontact(Request $req)
    {
    	$e = $req->email;
    	$m = $req->messaggio;

    	Mail::to($e)->send(new NewContact($m));

    	return redirect()->route("home")->with(["status"=>"success","message"=>"Grazie per averci contattati, ti risponderemo a breve!"]);
    }
}
