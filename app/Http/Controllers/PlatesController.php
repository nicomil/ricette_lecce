<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PlatesController extends Controller
{
    public function showplates(Request $req)
    {

    	$plates = session("piatti");
   	
    	return view("plates",compact("plates"));
    }

    public function showplate($idplate)
    {	
    	$p = null;
    	$plates = session('piatti');

    	foreach ($plates as $key => $p) {
    		if($p["id"]==$idplate)
    			$plate = $p;
    	}

    	return view("plate",compact("plate"));
    }

    public function addplate(Request $req)
    {

    	if(empty($req->session()->get("piatti"))){
    		$req->session()->put('piatti', []);
    	}

    	$piatti = $req->session()->get("piatti");
    	
    	$piatto = 
    	[
    	 "id"=>count($piatti),
	     "nome" => $req->input("piatto"),
	     "ingredienti" => explode(",",$req->input("ingredienti")),
	     "immagine"=>$req->input("immagine")
	 	];

	 	$piatti[] = $piatto;

    	$req->session()->put('piatti', $piatti);
    	$req->session()->put('n_piatti', count($piatti));

    	
    	return redirect()->route("piatti");
    }

    public function formplate()
    {
    	return view("form");
    }
    public function search(Request $req)
    {
    	$piatti = session("piatti");
    	foreach ($piatti as $piatto) {
    		if($req->piatto === $piatto['nome'] || $req->piatto === (string)$piatto['id'])
    			// return redirect("/plate/".$piatto['id']);
    			return redirect()->route("vedipiatto",["id"=>$piatto['id']]);
    	}

    	//$req->session()->flash('status', 'Piatto non trovato :(');
    	return redirect()->route("home")->with(['status'=>'danger','message'=>'Piatto non trovato :(']);
    }
    public function flush(){
    	session(["piatti"=>[]]);
    	return redirect("/plates");
    }
}
